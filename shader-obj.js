
var ShaderUniformName = {
	ModelViewProjectionMatrix : 0,
	ModelViewMatrix : 1,
	ViewMatrix : 2,
	NormalMatrix : 3,
	VertexColour : 4,
};

var ShaderUniformType = {
	UniformType_Vec2 : 0,
	UniformType_Vec3 : 1,
	UniformType_Vec4 : 2,

	UniformType_Mat4 : 3,
};

var AllUniforms = [
	{
		uniformIdent: ShaderUniformName.ModelViewProjectionMatrix,
		uniformType: ShaderUniformType.UniformType_Mat4,
		uniformLocation: -1,
		uniformString: "ogl_ModelViewProjectionMatrix"
	},
	
	{
		uniformIdent: ShaderUniformName.ModelViewMatrix,
		uniformType: ShaderUniformType.UniformType_Mat4,
		uniformLocation: -1,
		uniformString: "ogl_ModelViewMatrix"
	},	
	
	{
		uniformIdent: ShaderUniformName.ViewMatrix,
		uniformType: ShaderUniformType.UniformType_Mat4,
		uniformLocation: -1,
		uniformString: "ogl_ViewMatrix"
	},		
	
	{
		uniformIdent: ShaderUniformName.NormalMatrix,
		uniformType: ShaderUniformType.UniformType_Vec4,
		uniformLocation: -1,
		uniformString: "ogl_NormalMatrix"
	},
	
	{
		uniformIdent: ShaderUniformName.VertexColour,
		uniformType: ShaderUniformType.UniformType_Mat4,
		uniformLocation: -1,
		uniformString: "ogl_VertexColour"
	},	
];

function SetUniform ( uniformName, value ) {
	var u;
	
	if( (typeof uniformName === 'string' ) ) {
		u = this.uniformMap[uniformName];
	} else {
		u = this.uniformMap[uniformName.toString()];
	}
	
	if( u === null ||
		u === undefined ) {
		return;
	}
		
	var loc = u.uniformLocation;
	var type = u.uniformType;
		
	if( loc != -1 ) {
		if( type == ShaderUniformType.UniformType_Vec2 ) {
			gl.uniform2fv( loc, value.elements );
		} else if( type == ShaderUniformType.UniformType_Vec3 ) {
			gl.uniform3fv( loc, value.elements );
		} else if( type == ShaderUniformType.UniformType_Vec4 ) {
			gl.uniform4fv( loc, value.elements );
		} else if( type == ShaderUniformType.UniformType_Mat4 ) {
			gl.uniformMatrix4fv( loc, gl.GL_FALSE, value.elements );
		}				
	}
}
	
function Bind() {
	// bind the shader program
	if( this.programId != -1 ) {
		this.previousProgramId = gl.getParameter( gl.CURRENT_PROGRAM );
		gl.useProgram(this.programId);
	}
}
	
function UnBind () {
	// unbind and return to previous program if it existed
	if( this.previousProgramId != -1 ) {
		gl.useProgram(this.previousProgramId);
	} else {
		gl.useProgram(0);
	}
}
	
function Shader(gl, vertexShader, fragmentShader, geometryShader) {
	this.gl = gl;
	this.programId = -1;
	this.previousProgramId = 1;		
	this.uniformMap = {};		

	// create the program
	this.programId = loadShadersForProgram( gl, vertexShader, fragmentShader, geometryShader );
	
	// cache uniforms
	if( this.programId != -1 ) {
		for( var i=0; i < AllUniforms.length; ++i ) {
		
			var u = AllUniforms[i];
			u.uniformLocation = gl.getUniformLocation( this.programId, u.uniformString );

			// add to the map array
			
			this.uniformMap[u.uniformString] = u;
			this.uniformMap[u.uniformIdent.toString()] = u;
		}
	}
	
	this.SetUniform = SetUniform;
	this.Bind = Bind;
	this.UnBind = UnBind;
}

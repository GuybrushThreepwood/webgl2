
var shaderMap = [];

function clearShaderMap() {
	
	var len = shaderMap.length;
	
	// loop over the array and splice
	while( i-- ) {
		
		var currShader = shaderMap[i];
		
		if( currShader.program === programId ) {
			
			if( gl.isShader(currShadervertexShader) )
			{
				gl.detachShader(programId, currShader.vertexShader);
				gl.deleteShader(currShader.vertexShader);
			}

			if( gl.isShader(currShader.fragmentShader) )
			{
				gl.detachShader(programId, currShader.fragmentShader);
				gl.deleteShader(currShader.fragmentShader);
			}

			if (gl.isShader(currShader.geometryShader))
			{
				gl.detachShader(programId, currShader.geometryShader);
				gl.deleteShader(currShader.geometryShader);
			}
			
			if( gl.dsProgram( programId ) ) {
				gl.deleteProgram( programId );
			}	
			
			shaderMap.splice(i, 1);
		}
	}

}

function removeShaderProgram( gl, programId ) {
	
	// find and remove the shader program
	for( var i=0; i < shaderMap.length; ++i ) {
		
		var currShader = shaderMap[i];
		
		if( currShader.program === programId ) {
			
			// remove all attachments
			if( gl.isShader(currShadervertexShader) )
			{
				gl.detachShader(programId, currShader.vertexShader);
				gl.deleteShader(currShader.vertexShader);
			}

			if( gl.isShader(currShader.fragmentShader) )
			{
				gl.detachShader(programId, currShader.fragmentShader);
				gl.deleteShader(currShader.fragmentShader);
			}

			if (gl.isShader(currShader.geometryShader))
			{
				gl.detachShader(programId, currShader.geometryShader);
				gl.deleteShader(currShader.geometryShader);
			}
			
			if( gl.isProgram( programId ) ) {
				gl.deleteProgram( programId );
			}	
			
			shaderMap.splice(i, 1);
			
			return;
		}
	}	
}

function printInfoLog( gl, objectId ) {
	
	var isValid = true;
	var infoLog;
	var isShader = true;
	
	// determine if it's a shader or a program
	if ( !(objectId instanceof WebGLShader) )
		isShader = false;
	
	// determine if object is a shader or a program
	if( isShader ) {
		isValid = gl.getShaderParameter( objectId, gl.COMPILE_STATUS );
	} else {
		isValid = gl.getProgramParameter( objectId, gl.VALIDATE_STATUS );
	}		
	
	// something is wrong with the object
	if( !isValid ) {
		if( isShader ) {
			// shader errors
			infoLog = gl.getShaderInfoLog( objectId );
			console.log( 'SHADER Info Log:' );
		} else {
			// program errors
			infoLog = gl.getProgramInfoLog( objectId );
			console.log( 'PROGRAM Info Log:' );			
		}
		
		console.log(infoLog);
	}
	
	return isValid;
}

function loadShader( gl, shaderType, shaderString ) {
	
	// empty shader string is no good
	if( !shaderString ||
		shaderString.length === 0 ) {
			
		console.log( 'loadShader() ERROR: empty string' );
		return -1;
	}
	
	// check supported shader types
	if( shaderType != gl.VERTEX_SHADER &&
		shaderType != gl.FRAGMENT_SHADER &&
		shaderType != gl.GEOMETRY_SHADER ) {
		console.log( 'loadShader() ERROR: invalid shader type' );
		return -1;		
	}

	// create a new shader object
	var shaderId;
	shaderId = gl.createShader(shaderType);
	
	// attach the source and compile
	gl.shaderSource( shaderId, shaderString );
	gl.compileShader( shaderId );
	
	var isValid = printInfoLog( gl, shaderId );
	
	if(!isValid)
		return -1;
	
	return shaderId;
}

function loadShadersForProgram( gl, vertexShader, fragmentShader, geometryShader ) {
	var vsId = -1;
	var fsId = -1;
	var gsId = -1;
		
	// load a vertex shader
	if( vertexShader &&
		vertexShader.length != 0 ) {	
		vsId = loadShader( gl, gl.VERTEX_SHADER, vertexShader );
	}
	
	// load a fragment shader
	if( fragmentShader &&
		fragmentShader.length != 0 ) {	
		fsId = loadShader( gl, gl.FRAGMENT_SHADER, fragmentShader );
	}

	// load a geometry shader
	if( geometryShader &&
		geometryShader.length != 0 ) {	
		gsId = loadShader( gl, gl.GEOMETRY_SHADER, geometryShader );
	}	
	
	// valid ids to load into a program
	if( vsId != -1 ||
		fsId != -1 ||
		gsId != -1 ) {
	
		var programId = -1;
		programId = gl.createProgram();
		
		if( vsId != -1 )
			gl.attachShader( programId, vsId );

		if( fsId != -1 )
			gl.attachShader( programId, fsId );

		if( gsId != -1 )
			gl.attachShader( programId, gsId );
		
		gl.linkProgram( programId );
		
		var isLinked = false;
		isLinked = gl.getProgramParameter( programId, gl.LINK_STATUS );
		
		// DEBUG **********
		var validationStatus;
		gl.validateProgram(programId);
		printInfoLog(gl, programId);
		// ****************
		
		if(!isLinked)
			return -1;
		
		var completeShader = {
			program: programId,
			vertexShader: vsId,
			fragmentShader: fsId,
			geometryShader: gsId,			
		};

		shaderMap.push(completeShader);
		
		return programId;
	}
	
	return -1;
}
